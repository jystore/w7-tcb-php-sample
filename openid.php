<?php

include __DIR__.'/vendor/autoload.php';
/**
 * oauth 认证 获取openid
 */
$code = 'xxxx';//前端传递的code

$result = (new \W7\Sdk\OpenCloud\Service\Oauth\User())
    ->withAppId(getenv('APP_ID'))
    ->withAppSecret(getenv('APP_SECRET'))
    ->getUserInfoByIsCode(compact('code'));
$openId = data_get($result, 'open_id', null); //用户openid