<?php

use W7\Demo\Util\Http;

include __DIR__.'/vendor/autoload.php';
include __DIR__.'/Util/Http.php';
function sign($appid, $appsecret, $code) : array
{

    $params = [
        'appid' => $appid,
        'timestamp' => time(),
        'nonce' => random_int(1000, 10000).'',
        'code' => $code
    ];

    ksort($params);
    reset($params);
    $params = array_map(function ($value){
        return is_null($value) ? '' : $value;
    }, $params);
    $sign = md5(http_build_query($params). $appsecret);
    $params['sign'] = $sign;
    return $params;
}

function getOpenId($params = [])
{
    $response  = \Util\Http::post('https://openapi.w7.cc/we7/open/oauth/user/info/with-js-code', $params);
    var_dump($response->getBody()->getContents());
    $json = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
    return isset($json['open_id']) ? $json['open_id'] : [];
}

$code = 'ngMq52KGqQ7eM8K12y5e21QKEMq72ELy';
$appid ='292986';
$appsecret = 'fec6be9f866daab90478aa0e2a5f9158';

$params = sign($appid, $appsecret, $code);
$code = getOpenId($params);



